// ------------------------------------------------------------------------------
// Twitter : @JeffProd
// Web     : https://jeffprod.com
// ------------------------------------------------------------------------------

const url = require('url')
const path = require('path')
const { app, BrowserWindow, ipcMain } = require('electron')
var kill  = require('tree-kill');

let mainWindow = null
let childWindow = null

var ressourcePath = '/app/ressources';
const mainUrl = url.format({
  protocol: 'file',
  slashes: true,
  pathname: path.join(__dirname, 'app/index.html')
})

app.on('ready', function () {
 var jarPath_registry = app.getAppPath() + ressourcePath +'/registry.jar --spring.profiles.active=prod --spring.security.user.password=admin --jhipster.security.authentication.jwt.secret=my-secret-key-which-should-be-changed-in-production-and-be-base64-encoded --spring.cloud.config.server.composite.0.type=git --spring.cloud.config.server.composite.0.uri=https://github.com/jhipster/jhipster-registry-sample-config';
  var jarPath_uaa = app.getAppPath() +ressourcePath +'/mena-uaa.war';
  var jarPath_protail = app.getAppPath() + ressourcePath+ '/portal.war'; 
  var jarPath_gatway = app.getAppPath() + ressourcePath+'/gatewaybac.war';
  var jarPath_microservice = app.getAppPath() + ressourcePath+'/microservicebac.war';
  var  child_registy = null;
  var child_uaa = null;
  var child_gatway = null;
  var child_microservice = null;

  var mainWindow = new BrowserWindow({
    center: true,
    minWidth: 1024,
    minHeight: 768,
    show: false
  })

 async function  excujar() {
  child_registy = await require('child_process').spawn(
    'java', ['-jar', jarPath_registry, ''], { shell: true });

       child_registy.stdout.on('data', function (data) {
             console.log('stdout: ' + data);

         });
  
  // if (child_registy.pid != null){

    setTimeout(()=>{

      child_uaa  = require('child_process').spawn(
        'java', ['-jar', jarPath_uaa, ''], { shell: true }
    );
  
    child_portail = require('child_process').spawn(
        'java', ['-jar', jarPath_protail, ''], { shell: true }
    ); 
  
    
    child_gatway = require('child_process').spawn(
      'java', ['-jar', jarPath_gatway, ''], { shell: true }
  );
  
    child_microservice = require('child_process').spawn(
      'java', ['-jar', jarPath_microservice, ''], { shell: true }
  );
    }, 40000)
   
  // }

}

  

//   child_registy.stdout.on('data', function (data) {
//       console.log('stdout: ' + data);

//   });

//   child_uaa.stdout.on('data', function (data) {
//     console.log('stdout: ' + data);

//   });

//  child_portail.stdout.on('data', function (data) {
//   console.log('stdout: ' + data);

//  });
    
  

  // mainWindow.webContents.openDevTools()
  

  
function createWindow() {
  // Create the browser window.
  // const mainWindow = new BrowserWindow({
  //   width: 800,
  //   height: 600,
  //   show: false,
  //   webPreferences: {
  //     preload: path.join(__dirname, 'preload.js')
  //   }
  // })
  // mainWindow.loadFile('index.html')
  excujar();

 

  setTimeout(() => {
    mainWindow.loadURL(mainUrl)
  }, 59000);
  

  mainWindow.webContents.on('dom-ready', function () {
    console.log('user-agent:', mainWindow.webContents.getUserAgent());
    //mainWindow.webContents.openDevTools()
    mainWindow.maximize()
    mainWindow.show()
  })

  // and load the index.html of the app.
  var splash = new BrowserWindow({ 
    width: 500, 
    height: 300, 
    transparent: true, 
    frame: false, 
    alwaysOnTop: true 
  });
  
  
  splash.loadFile('splash.html');
  splash.center();
  setTimeout(function () {
    splash.close();
    mainWindow.center();
    mainWindow.show();
  }, 50000);


  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

app.whenReady().then(() => {



  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})


  mainWindow.on('closed', function () {
    kill(child_registy.pid);
    kill(child_uaa.pid);
    kill(child_portail.pid);
    kill(child_gatway.pid);
    kill(child_microservice.pid);
    mainWindow = null
    app.quit()
  })

  // childWindow = new BrowserWindow({
  //     parent: mainWindow,
  //     center: true,
  //     minWidth: 800,
  //     minHeight: 600,
  //     show: true,
  //     webPreferences: {
  //       nodeIntegration: false, // https://electronjs.org/docs/tutorial/security#2-d%C3%A9sactiver-lint%C3%A9gration-de-nodejs-dans-tous-les-renderers-affichant-des-contenus-distants
  //       preload: path.join(__dirname, 'app/js/preload.js')
  //     }
  // })

  // childWindow.webContents.openDevTools()
  // childWindow.webContents.on('dom-ready', function () {
  //   console.log('childWindow DOM-READY => send back html')
  //   childWindow.send('sendbackhtml')
  // })
}) // app.on('ready'

// Quit when all windows are closed.
// app.on('window-all-closed', function () {
//   if (process.platform !== 'darwin') { app.exit() }
// })

ipcMain.on('scrapeurl', (event, url) => {
  childWindow.loadURL(url, { userAgent: 'My Super Browser v2.0 Youpi Tralala !' })
})

ipcMain.on('hereishtml', (event, html) => {
  mainWindow.send('extracthtml', html)
})
